import daisyui from 'daisyui'
module.exports = {
  content: [
    "./src/**/*.{html,ts}",
  ],
  theme: {
    extend: {},
  },
  daisyui: {
    themes: ["retro"]
  },
  plugins: [
    daisyui
  ],
}
