import { HttpRequest, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable()
export class TokenService {
  private token: string | undefined;

  setTokenFromResponse(response: HttpResponse<any>) {
    const auth = response.headers.get('Authorization');

    if (auth) {
      const bearer = auth.split(' ')[1];
      if (bearer) {
        this.token = bearer;
      } else {
        this.token = undefined;
      }
    }
  }

  updateRequestHeaders(headers: any) : HttpRequest<any> {
    if (this.token) {
      return headers.set('Authorization', `Bearer ${this.token}`);
    }
    return headers;
  }

  clearToken() {
    this.token = undefined;
  }
}
