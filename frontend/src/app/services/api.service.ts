import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable()
export class ApiService {

  constructor(private http:HttpClient) { }

  register(data: any) : Observable<any> {
    return this.http.post('/api/auth/register', data);
  }

  login(data: any) : Observable<any> {
    return this.http.post('/api/auth/login', data);
  }
}
