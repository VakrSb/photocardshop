import { Injectable, inject } from "@angular/core";
import { CanActivateFn, Router } from "@angular/router";
import { Select } from "@ngxs/store";
import { AccountState } from "../modules/account/account.state";
import { Observable } from "rxjs";

@Injectable(
  { providedIn: 'root' }
)
class PermissionService {
  @Select(AccountState.isAuthenticated)
  declare isAuthenticated$: Observable<boolean>;

  get canActivate(): boolean {
    let isAuthenticated = false;
    const sub = this.isAuthenticated$.subscribe((isAuth) => isAuthenticated = isAuth);
    sub.unsubscribe();
    return isAuthenticated;
  }
}

export const AuthGuard: CanActivateFn = (route, state) => {
  const router = inject(Router);
  const guardCheck = inject(PermissionService);

  if(guardCheck.canActivate) {
    return true;
  }

  return router.navigateByUrl('/account/login');
}
