import { Injectable, inject } from "@angular/core";
import { CanActivateFn } from "@angular/router";
import { Select } from "@ngxs/store";
import { AccountState } from "../modules/account/account.state";
import { Observable } from "rxjs";

@Injectable(
  { providedIn: 'root' }
)
class PermissionService {
  @Select(AccountState.isAuthenticated)
  declare isAuthenticated$: Observable<boolean>;

  get canActivate(): boolean {
    let isAuthenticated = false;
    this.isAuthenticated$.subscribe((isAuth) => isAuthenticated = isAuth).unsubscribe();
    return isAuthenticated;
  }
}

export const GuestGuard: CanActivateFn = (route, state) => {
  return !inject(PermissionService).canActivate;
}
