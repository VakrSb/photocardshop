import { ApplicationConfig, importProvidersFrom } from '@angular/core';
import { provideRouter } from '@angular/router';

import { routes } from './app.routes';
import { HTTP_INTERCEPTORS, provideHttpClient, withInterceptorsFromDi,} from '@angular/common/http';
import { NgxsModule } from '@ngxs/store';
import { AuthInterceptor } from './auth.interceptor';
import { CartState } from './modules/cart/cart.state';

export const appConfig: ApplicationConfig = {
  providers: [
    provideRouter(routes),
    provideHttpClient(withInterceptorsFromDi()),
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true,
    },
    importProvidersFrom(
      NgxsModule.forRoot([CartState]), // est ce qu'il y a moyen de ne pas le load ici mais dans le module cart ?
    ),
  ]
};
