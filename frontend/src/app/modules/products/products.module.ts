import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProductListComponent } from './components/product-list/product-list.component';
import { ProductAlbumComponent } from './components/product-album/product-album.component';
import { ProductsRoutingModule } from './products.routes';
import { ProductSearchComponent } from './components/product-search/product-search.component';



@NgModule({
  declarations: [
    ProductListComponent,
    ProductAlbumComponent,
    ProductSearchComponent
  ],
  imports: [
    CommonModule,
    ProductsRoutingModule
  ]
})
export class ProductsModule { }
