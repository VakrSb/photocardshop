import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Album } from '../models/album';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  constructor(private http:HttpClient) { }

  getPhotocards() : Observable<Album[]> {
    return this.http.get<Album[]>('/api/products');
  }
  search(search: string) : Observable<Album[]> {
    return this.http.get<Album[]>('/api/products/search', { params: { search: search } });
  }
}
