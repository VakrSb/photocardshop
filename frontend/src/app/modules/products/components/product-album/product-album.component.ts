import { Component, Input } from '@angular/core';
import { Album } from '../../models/album';
import { Store } from '@ngxs/store';
import { AddItem } from '../../../cart/cart.actions';

@Component({
  selector: 'product-album',
  templateUrl: './product-album.component.html',
})
export class ProductAlbumComponent {
  @Input() album: Album;

  constructor(private store: Store) {}

  public addToCart(album: Album) {
    const item = {
      name: album.title,
      price: album.price,
      quantity: 1,
    };
    this.store.dispatch(new AddItem(item));
  }
}
