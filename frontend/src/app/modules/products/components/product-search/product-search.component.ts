import { Component, EventEmitter, Output } from '@angular/core';
import { Subject, Subscription, debounceTime, distinctUntilChanged } from 'rxjs';

@Component({
  selector: 'product-search',
  templateUrl: './product-search.component.html',
})
export class ProductSearchComponent {
  @Output() onChange : EventEmitter<string> = new EventEmitter<string>();

  private inputValue: Subject<string>;

  private subscription: Subscription;

  ngOnInit() {
    this.inputValue = new Subject<string>();
    this.subscription = this.inputValue.pipe(
      debounceTime(300),
      distinctUntilChanged()
    ).subscribe(value => this.onChange.emit(value));
  }
  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  onInput(e: any) {
    this.inputValue.next(e.target.value);
  }
}
