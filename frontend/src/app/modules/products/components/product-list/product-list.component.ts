import { Component } from '@angular/core';
import { BehaviorSubject, Observable, catchError, of, share, switchMap } from 'rxjs';
import { Album } from '../../models/album';
import { ProductService } from '../../services/product.service';

@Component({
  selector: 'product-list',
  templateUrl: './product-list.component.html',
})
export class ProductListComponent {
  constructor(private service: ProductService) {}

  private searchValue: BehaviorSubject<string> = new BehaviorSubject('');
  protected cards$: Observable<Album[]>;

  ngOnInit() {
    this.cards$ = this.searchValue.pipe(
      switchMap(search => this.service.search(search)),
      catchError(err => {
        return of([]);
      }),
    );
  }

  protected onSearchChange(e: any) {
    this.searchValue.next(e);
  }
}
