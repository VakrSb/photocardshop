export type Album = {
  artist: string;
  title: string;
  year: number;
  cover: string;
  price: number;
}
