import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { CardsListComponent } from "./components/cards-list/cards-list.component";

export const routes: Routes = [
  {
    path: '',
    component: CardsListComponent
  },
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
  ],
  exports: [RouterModule]
})
export class CardsRoutingModule { }
