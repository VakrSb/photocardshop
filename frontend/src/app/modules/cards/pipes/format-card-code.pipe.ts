import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'formatCardCode',
  standalone: true
})
export class FormatCardCodePipe implements PipeTransform {

  transform(value: unknown, ...args: unknown[]): unknown {
    return (value as string).toString().replace(/(\d{4})/g, '$1 ').trim();
  }

}
