import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CardsListComponent } from './components/cards-list/cards-list.component';
import { CardsItemComponent } from './components/cards-item/cards-item.component';
import { CardsRoutingModule } from './cards.routes';
import { FormatCardCodePipe } from './pipes/format-card-code.pipe';
import { CardFormComponent } from './components/card-form/card-form.component';
import { ReactiveFormsModule } from '@angular/forms';



@NgModule({
  declarations: [
    CardsListComponent,
    CardsItemComponent,
    CardFormComponent
  ],
  imports: [
    FormatCardCodePipe,
    CommonModule,
    ReactiveFormsModule,
    CardsRoutingModule
  ]
})
export class CardsModule { }
