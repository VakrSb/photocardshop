export class Card {
  name: string;
  code: string;
  ccv: string;
  date: string;
}
