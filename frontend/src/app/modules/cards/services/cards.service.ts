import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Card } from '../models/card';

@Injectable({
  providedIn: 'root'
})
export class CardsService {

  constructor(private http:HttpClient) { }

  public getCards() : Observable<Card[]> {
    return this.http.get<Card[]>('/api/cards');
  }

  public addCard(card: Card) : Observable<Card> {
    return this.http.post<Card>('/api/cards', card);
  }
}
