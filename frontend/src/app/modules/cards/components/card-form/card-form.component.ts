import { Component } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { CardsService } from '../../services/cards.service';
import { Card } from '../../models/card';

@Component({
  selector: 'app-card-form',
  templateUrl: './card-form.component.html',
})
export class CardFormComponent {
  public constructor(
    private formBuilder: FormBuilder,
    private api: CardsService,
  ) {}

  protected form = this.formBuilder.group({
    name: ['', Validators.required , Validators.minLength(3)],
    code: ['', Validators.required,],
    ccv: ['', Validators.required],
    date: ['', Validators.required],
  });
  protected hasError(controlName: string) {
    const control = this.form.get(controlName);
    return control!.touched && control!.invalid;
  }
  protected onSubmit() {
    if (this.form.invalid) {
      return;
    }
    this.api.addCard(this.form.value as Card).subscribe(
      response => {
          console.log(response);
      },
      error => {
        console.log(error);
      }
    ).unsubscribe();
  }
}
