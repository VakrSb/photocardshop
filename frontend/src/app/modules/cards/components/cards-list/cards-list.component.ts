import { Component } from '@angular/core';
import { Observable } from 'rxjs';
import { Card } from '../../models/card';
import { CardsService } from '../../services/cards.service';

@Component({
  selector: 'app-cards-list',
  templateUrl: './cards-list.component.html',
})
export class CardsListComponent {
  cards: Observable<Card[]>;

  constructor(private cardsService: CardsService) {}

  ngOnInit() {
    this.cards = this.cardsService.getCards();
  }
}
