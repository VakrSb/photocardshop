import { Component } from '@angular/core';
import { CartState } from '../../cart.state';
import { Observable } from 'rxjs';
import { CartItem } from '../../models/cart-item';
import { Select, Store } from '@ngxs/store';
import { ClearCart, RemoveItem } from '../../cart.actions';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
})
export class CartComponent {
  constructor(private store: Store) {}

  @Select(CartState.getItems) items$?: Observable<CartItem[]>;
  @Select(CartState.getNumberOfItems) numberOfItems$?: Observable<number>;

  protected alert = false;
  protected error = false;


  protected command() {
    this.numberOfItems$?.subscribe((items) => {
      if (items === 0) {
        this.error = true;
        setTimeout(() => {
          this.error = false;
        }, 3000);
        return;
      }
      else {
        this.alert = true;
        // wait 3 seconds and hide the alert
        setTimeout(() => {
          this.alert = false;
        }, 3000);
      }
    }).unsubscribe();
    this.store.dispatch(new ClearCart());
  }

  public remove(item: CartItem) {
    this.store.dispatch(new RemoveItem(item));
  }
}
