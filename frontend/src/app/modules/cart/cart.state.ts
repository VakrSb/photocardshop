import { Action, Selector, State, StateContext } from "@ngxs/store";
import { Injectable } from "@angular/core";
import { Cart } from "./models/cart";
import { AddItem, ClearCart, RemoveItem } from "./cart.actions";
import { CartItem } from "./models/cart-item";

export interface CartStateModel {
  items: CartItem[];
}

@State<CartStateModel>({
  name: "cart",
  defaults: {
    items: [],
  },
})

@Injectable()
export class CartState {
  @Selector()
  static getItems(state: CartStateModel) {
    return state?.items;
  }

  @Selector()
  static getNumberOfItems(state: CartStateModel) {
    return state.items.length;
  }

  @Action(RemoveItem)
  remove(
    { patchState, getState }: StateContext<CartStateModel>,
    { payload }: RemoveItem
  ) {
    const state = getState();
    const items = state.items.filter((i: any) => i !== payload);
    patchState({
      items,
    });
  }

  @Action(AddItem)
  add(
    { getState, patchState }: StateContext<CartStateModel>,
    { payload }: AddItem
  ) {
    const state = getState();
    if (state.items.some((i: any) => i.name === payload.name)) {
      state.items.forEach((i: any) => {
        if (i.name === payload.name) {
          i.quantity += payload.quantity;
        }
      });
    } else {
      state.items.push(payload);
    }
    patchState({
        items: state.items,
    });
  }

  @Action(ClearCart)
  clear({ patchState }: StateContext<CartStateModel>) {
    patchState({
      items: [],
    });
  }
}
