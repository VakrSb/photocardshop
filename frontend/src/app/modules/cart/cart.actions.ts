import { CartItem } from "./models/cart-item";

export class RemoveItem {
  static readonly type = '[Cart] Remove Item';
  constructor(public payload: CartItem) {}
}

export class AddItem {
  static readonly type = '[Cart] Add Item';
  constructor(public payload: CartItem) {}
}

export class ClearCart {
  static readonly type = '[Cart] Clear Cart';
}
