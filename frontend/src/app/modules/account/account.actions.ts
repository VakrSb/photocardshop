import { Account } from "./models/account";

export class SetAccount {
  static readonly type = '[Account] Set Account';
  constructor(public account: Account | null) {}
}

export class SetToken {
  static readonly type = '[Account] Set Token';
  constructor(public token: string | undefined) {}
}
