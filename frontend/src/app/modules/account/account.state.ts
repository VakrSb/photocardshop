import { Action, Selector, State, StateContext } from "@ngxs/store";
import { Account } from "./models/account";
import { Injectable } from "@angular/core";
import { SetAccount, SetToken } from "./account.actions";

export interface AccountStateModel {
  token: string | undefined;
  account: Account | null;
}

@State<AccountStateModel>({
  name: 'account',
  defaults: {
    token: '',
    account: null,
  },
})
@Injectable()
export class AccountState {

  @Selector()
  static isAuthenticated(state: AccountStateModel) {
    return !!state.token;
  }

  @Selector()
  static getToken(state: AccountStateModel) {
    return state.token;
  }

  @Action(SetAccount)
  setAccount(
    { patchState }: StateContext<AccountStateModel>,
    { account }: SetAccount
  ) {
    patchState({ account });
  }

  @Action(SetToken)
  setToken(
    { patchState }: StateContext<AccountStateModel>,
    { token }: SetToken
  ) {
    patchState({ token });
  }
}
