import { Component } from '@angular/core';
import { AbstractControl, FormBuilder, Validators } from '@angular/forms';
import { ApiService } from '../../../../services/api.service';
import { Store } from '@ngxs/store';
import { SetAccount } from '../../account.actions';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  providers: [ApiService],
})
export class RegisterComponent {
  public constructor(
    private formBuilder: FormBuilder,
    private api: ApiService,
    private store: Store,
    private router: Router
  ) {}
  protected error: string = '';
  protected form = this.formBuilder.group({
    firstName: this.formBuilder.control('', [
      Validators.required,
    ]),
    lastName: this.formBuilder.control('', [
      Validators.required,
    ]),
    address: this.formBuilder.control('', [
      Validators.required,
    ]),
    postalCode: this.formBuilder.control('', [
      Validators.required,
      Validators.pattern('[0-9]{5}'),
    ]),
    city: this.formBuilder.control('', [
      Validators.required,
    ]),
    phone: this.formBuilder.control('', [
      Validators.required,
      Validators.pattern('[0-9]{10}'),
    ]),
    email: this.formBuilder.control('', [
      Validators.required,
      Validators.email,
    ]),
    title: this.formBuilder.control('', [
      Validators.required,
    ]),
    country: this.formBuilder.control('', [
      Validators.required,
    ]),
    password: this.formBuilder.control('', [
      Validators.required,
      Validators.minLength(8),
    ]),
    confirmPassword: this.formBuilder.control('', [
      Validators.required,
    ]),
  },
    {
      validators: (group: AbstractControl) => {
        const password = group.get('password');
        const confirmPassword = group.get('confirmPassword');
        if (password!.value !== confirmPassword!.value) {
          confirmPassword!.setErrors({ notSame: true });
        }
        return null;
      }
    });
  protected hasError(controlName: string) {
    const control = this.form.get(controlName);
    return control!.touched && control!.invalid;
  }
  protected onSubmit() {
    if (this.form.invalid) {
      return;
    }
    this.api.register(this.form.value).subscribe(
      response => {
        this.store.dispatch(new SetAccount({
          email: response.email,
        }));
        this.router.navigate(['/']);
      },
      error => {
        this.error = error.error;
      }
    ).unsubscribe();
  }
}
