import { Component } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ApiService } from '../../../../services/api.service';
import { Store } from '@ngxs/store';
import { Router } from '@angular/router';
import { SetAccount } from '../../account.actions';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  providers: [ApiService],
})
export class LoginComponent {
  public constructor(
    private formBuilder: FormBuilder,
    private api: ApiService,
    private store: Store,
    private router: Router
  ) {}

  protected error: string = '';

  protected form = this.formBuilder.group({
    email: this.formBuilder.control('',[
      Validators.required,
      Validators.email,
    ]),
    password: this.formBuilder.control('',[
      Validators.required,
    ]),
  });
  protected hasError(controlName: string) {
    const control = this.form.get(controlName);
    return control!.touched && control!.invalid;
  }
  protected onSubmit() {
    if (this.form.invalid) {
      return;
    }
    this.api.login(this.form.value).subscribe(
      response => {
          this.store.dispatch(new SetAccount({
            email: response.email,
          }));
          this.router.navigate(['/']);
      },
      error => {
        this.error = error.error;
      }
    );
  }
}
