import { Component } from '@angular/core';
import { Router, RouterLink } from '@angular/router';
import { Select, Store } from '@ngxs/store';
import { AccountState } from '../../../modules/account/account.state';
import { Observable } from 'rxjs';
import { CommonModule } from '@angular/common';
import { SetToken } from '../../../modules/account/account.actions';
import { CartState } from '../../../modules/cart/cart.state';
import { ClearCart } from '../../../modules/cart/cart.actions';

@Component({
  selector: 'app-header',
  standalone: true,
  imports: [RouterLink, CommonModule],
  templateUrl: './header.component.html',
})
export class HeaderComponent {
  constructor(
    private store: Store,
    private router: Router
  ) {}
  @Select(CartState.getNumberOfItems) numberOfItems$?: Observable<number>;

  @Select(AccountState.isAuthenticated)
  declare isAuthenticated$: Observable<boolean>;

  get isAuthenticated() {
    return this.isAuthenticated$;
  }
  protected logout() {
    this.store.dispatch(new SetToken(undefined));
    this.store.dispatch(new ClearCart());
    this.router.navigate(['/']);
  }
}
