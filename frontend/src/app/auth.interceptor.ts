import { HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpInterceptorFn, HttpRequest, HttpResponse } from "@angular/common/http";
import { Store } from "@ngxs/store";
import { AccountState } from "./modules/account/account.state";
import { Injectable } from "@angular/core";
import { catchError, tap, throwError } from "rxjs";
import { SetToken } from "./modules/account/account.actions";

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  constructor(private store: Store) {}

  get token() {
    return this.store.selectSnapshot(AccountState.getToken);
  }

  set token(token: string | undefined) {
    this.store.dispatch(new SetToken(token));
  }

  intercept(req: HttpRequest<any>, next: HttpHandler) {
    if (this.token) {
      req = req.clone({
        setHeaders: {
          Authorization: `Bearer ${this.token}`,
        },
      });
    }
    return next.handle(req)
      .pipe(
        tap((event: HttpEvent<unknown>) => {
          if (event instanceof HttpResponse) {
            const auth = event.headers.get('Authorization');
            if (auth) {
              const bearer = auth.split(' ')[1];
              if (bearer) {
                this.token = bearer;
              } else {
                this.token = undefined;
              }
            }
          }
        }),
      )
      .pipe(
        catchError((error: HttpErrorResponse) => {
          switch (error.status) {
            case 401:
              this.token = undefined;
              break;
          }
          return throwError(() => error);
        }),
      );
  }
}
