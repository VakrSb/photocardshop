const configs = {
  development: {
    dialect: 'sqlite',
    storage: './db.development.sqlite',
    define: {
      timestamps: false,
    }
  },
  production: {
    dialect: "postgres",
    protocol: "postgres",
    dialectOptions: {
      ssl: true,
      native: true,
    },
    define: {
      timestamps: false,
    },
  },
};

export default configs;
