import express from 'express';
import { create, getAll } from '../controllers/cards.controller.js';
import { checkJwt } from '../middlewares/jwt.middleware.js';

var router = express.Router();
router.get("/", getAll);
router.post("/", create);

const setupRoutes = (app) => {
  app.use('/cards', checkJwt, router);
}

export default setupRoutes;
