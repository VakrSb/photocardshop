import express from 'express';
import { getAll, search } from '../controllers/products.controller.js';
import { checkJwt } from '../middlewares/jwt.middleware.js';

var router = express.Router();
router.get("/", getAll);
router.get("/search", search);

const setupRoutes = (app) => {
  app.use('/products', checkJwt, router);
}

export default setupRoutes;
