import {register,login} from '../controllers/auth.controller.js';
import express from 'express';

var router = express.Router();
router.post("/register", register);
router.post("/login", login);

const setupRoutes = (app) => {
  app.use('/auth', router);
}

export default setupRoutes;
