import { Op } from 'sequelize';
import db from '../models/index.js';

const albums = db.Album;

export function getAllAlbums() {
  return albums.findAll();
}

export function searchAlbums(query) {
  return albums.findAll({
    where: {
      [Op.or]: [
        {
          title: {
            [Op.like]: `%${query}%`
          }
        },
        {
          artist: {
            [Op.like]: `%${query}%`
          }
        }
      ]
    }
  });
}
