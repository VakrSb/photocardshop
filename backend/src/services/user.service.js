import db from '../models/index.js';
import bcrypt from 'bcrypt';

const users = db.User;

export function validateUser(user) {
  return users.findOne({
    where: {
      email: user.email
    }
  }).then((fuser) => {
    if (!fuser || !bcrypt.compareSync(user.password, fuser.password)) {
        return Promise.reject(new Error('Invalid user'));
    }
    return fuser;
  });
}


export function createUser(user) {
  return users.create({
    ...user,
    password: bcrypt.hashSync(user.password, 8),
  });
}
