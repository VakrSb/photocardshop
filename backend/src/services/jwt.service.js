import jwt from 'jsonwebtoken'

const JWT_ACCESS_EXPIRES_IN = '365d'

export function generateToken(user) {
  return jwt.sign({ user }, process.env.APP_KEY, {
    expiresIn: JWT_ACCESS_EXPIRES_IN,
  })
}
