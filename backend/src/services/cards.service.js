import db from '../models/index.js';

const cards = db.Card;

export function getAllCards(userId) {
  return cards.findAll({
    where: {
      userId: userId
    }
  });
}

export function createCard(card) {
  return cards.create(card);
}
