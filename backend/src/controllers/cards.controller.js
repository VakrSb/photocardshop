import { createCard, getAllCards } from "../services/cards.service.js"

export function getAll(req, res) {
  const userID = req.user.user.id;
  res.setHeader('Content-Type', 'application/json');
  getAllCards(userID).then((cards) => {;
    res.status(200).send(cards);
  })
  .catch((error) => {
    res.status(200).send([]);
  });
}

export function create(req, res) {
  const card = req.body;
  createCard(card).then((card) => {
    res.setHeader('Content-Type', 'application/json');
    res.status(201).send(card);
  }).catch((error) => {
    res.status(400).send({ message: "Error creating card" });
  });
}
