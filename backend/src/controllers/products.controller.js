import { getAllAlbums, searchAlbums } from "../services/albums.service.js"

export function getAll(req, res) {
  const products = getAllAlbums();
  res.setHeader('Content-Type', 'application/json');
  res.status(200).send(products);
}

export function search(req, res) {
  const query = req.query.search.toLowerCase();
  searchAlbums(query).then((products) => {
    res.setHeader('Content-Type', 'application/json');
    res.status(200).send(products);
  })
  .catch((error) => {
    res.status(400).send({ message: "No products available for this query" });
  });
}

