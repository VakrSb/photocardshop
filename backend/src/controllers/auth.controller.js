import { generateToken } from '../services/jwt.service.js';
import { validateUser, createUser } from '../services/user.service.js';

export function login(req, res) {
  const user = req.body;
  validateUser(user)
    .then((fuser) => {
      const token = generateToken(fuser);
      res.setHeader('Authorization', `Bearer ${token}`);
      res.status(200).send(fuser);
    })
    .catch((err) => {
      res.status(401).send('Invalid user');
    });
}

export function register(req, res) {
  const user = req.body;
  createUser(user)
    .then(() => {
      login(req, res);
    })
    .catch((err) => {
      res.status(400).send(err.message);
    });
}
