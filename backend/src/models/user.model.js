export default (sequelize, Sequilize) => {
  return sequelize.define('user', {
    id: {
      type: Sequilize.INTEGER,
      autoIncrement: true,
      primaryKey: true
    },
    email: {
      type: Sequilize.STRING,
      allowNull: false,
      validate: {
        isEmail: true
      },
      unique: true
    },
    password: {
      type: Sequilize.STRING,
      allowNull: false
    }
  });
}
