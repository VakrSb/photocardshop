export default (sequelize, Sequilize) => {
  return sequelize.define('album', {
    id: {
      type: Sequilize.INTEGER,
      autoIncrement: true,
      primaryKey: true
    },
    title: {
      type: Sequilize.STRING,
      allowNull: false
    },
    artist: {
      type: Sequilize.STRING,
      allowNull: false
    },
    year: {
      type: Sequilize.INTEGER,
      allowNull: false
    },
    price: {
      type: Sequilize.DECIMAL(10, 2),
      allowNull: false
    },
    cover : {
      type: Sequilize.STRING,
      allowNull: false
    }
  });
}
