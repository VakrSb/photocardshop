export default (sequelize, Sequilize) => {
  return sequelize.define('card', {
    id: {
      type: Sequilize.INTEGER,
      autoIncrement: true,
      primaryKey: true
    },
    name: {
      type: Sequilize.STRING,
      allowNull: false
    },
    code : {
      type: Sequilize.STRING,
      allowNull: false,
      unique: true
    },
    ccv: {
      type: Sequilize.STRING,
      allowNull: false
    },
    date: {
      type: Sequilize.STRING,
      allowNull: false
    }
  });
}
