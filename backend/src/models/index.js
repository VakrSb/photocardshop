import Sequelize from 'sequelize';
import bcrypt from 'bcrypt';

const env = process.env.NODE_ENV || 'development';
import configs from '../config/config.js';

import User from './user.model.js';
import Album from './album.model.js';
import Card from './cards.model.js';

const config = configs[env];

let sequelize;
if (env === 'production') {
  sequelize = new Sequelize(process.env.DATABASE_URL, config);
} else {
  sequelize = new Sequelize(config);
}

const db = {
  sequelize,
  Sequelize,
};

const models = {
  User: User,
  Album: Album,
  Card: Card,
};

Object.entries(models).forEach(([modelName, model]) => {
  db[modelName] = model(sequelize, Sequelize);
})

db.User.hasMany(db.Card);
db.Card.belongsTo(db.User);

export default db;

export function fillDatabase() {
  const User = db.User;
  User.findAll().then((users) => {
    if(users.length == 0){
      User.create({ email: "aa@aa.com", password: bcrypt.hashSync("12345678", 8)});
      User.create({ email: "johndoe@lecnam.net", password: bcrypt.hashSync("12345678", 8)});
    }
  });

  const Album = db.Album;
  Album.findAll().then((albums) => {
    if(albums.length == 0){
      Album.create({ title: "Drama", artist: "Aespa", year: 2023, price: 69, cover: "/api/covers/drama.png"});
      Album.create({ title: "++", artist: "Loona", year: 2018, price: 420, cover: "/api/covers/++.webp"});
      Album.create({ title: "Armageddon", artist: "Aespa", year: 2024, price: 42, cover: "/api/covers/Armageddon.jpg"});
      Album.create({ title: "Switch", artist: "IVE", year: 2024, price: 666, cover: "/api/covers/switch.jpg"});
    }
  });

  const Card = db.Card;
  Card.findAll().then((cards) => {
    if(cards.length == 0){
      Card.create({ name: "John", code: "6271701225979642", ccv: "520", date: "12/23", userId: 1});
      Card.create({ name: "Doe", code: "6271701225979643", ccv: "782", date: "06/26", userId: 2});
    }
  });
}
