import express from 'express';
import cors from 'cors';

import auth_routes from './src/routes/auth.routes.js';
import products_routes from './src/routes/products.routes.js';
import cards_routes from './src/routes/cards.routes.js';

import db, { fillDatabase } from './src/models/index.js';

db.sequelize
  .sync()
  .then(() => {
    fillDatabase();
    console.log('Database connected');
  })
  .catch((err) => {
    console.log('Error: ', err);
    process.exit(1);
  });

const app = express();

app.use(express.static('public'))

const corsOptions = {
    origin: 'http://localhost:3000',
}

app.use(cors(corsOptions));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.get('/', (_, res) => {
    res.send('Hello World!');
})

auth_routes(app);
products_routes(app);
cards_routes(app);

const PORT = process.env.PORT || 3000;
app.listen(PORT, () => {
    console.log(`Server is running on port ${PORT}`);
})
